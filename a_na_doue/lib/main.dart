import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:stacked_themes/stacked_themes.dart';
import 'package:get_storage/get_storage.dart';

import 'routes/app_pages.dart';

Future main() async {
  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();
  await ThemeManager.initialise();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return ThemeBuilder(
      statusBarColorBuilder: (theme) => theme.accentColor,
      defaultThemeMode: ThemeMode.dark,
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        scaffoldBackgroundColor: Colors.grey[900],
        backgroundColor: Colors.grey[900],
        fontFamily: 'Gl_two',
        accentColor: Colors.grey[900],
      ),
      lightTheme: ThemeData(
        brightness: Brightness.light,
        scaffoldBackgroundColor: Colors.white,
        backgroundColor: Colors.white,
        fontFamily: 'Gl',
        accentColor: Colors.white,
      ),
      builder: (context, regularTheme, darkTheme, themeMode) => GetMaterialApp(
        title: 'A Na Doue',
        debugShowCheckedModeBanner: false,
        theme: regularTheme,
        darkTheme: darkTheme,
        themeMode: themeMode,
        initialRoute: AppRoutes.INITIAL,
        getPages: AppRoutes.routes,
      ),
    );
  }
}
