import 'package:a_na_doue/ui/game_part.dart';
import 'package:a_na_doue/ui/home.dart';
import 'package:a_na_doue/ui/home_binding.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

part 'app_routes.dart';

class AppRoutes {
  static const INITIAL = Routes.HOME;

  static var userData = GetStorage();
  static String getInitialRoute() {
    userData.writeIfNull('isLogged', false);
    if (userData.read('isLogged')) {
      return Routes.HOME;
    }
    return Routes.HOME;
  }

  static var routes = [
    // Lead home page
    GetPage(
      name: Routes.HOME,
      page: () => HomePage(),
      binding: HomeBinding(),
    ),
    //Game part
    GetPage(
      name: Routes.PART,
      page: () => GamePart(),
      binding: HomeBinding(),
    ),
  ];
}
