import 'package:a_na_doue/ui/home_controller.dart';
import 'package:a_na_doue/ui/widget/life_progressor.dart';
import 'package:a_na_doue/ui/widget/time_progressor.dart';
import 'package:a_na_doue/utility/text_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'widget/buy_time.dart';
import 'widget/keyboard_widget.dart';
import 'widget/lead_button.dart';

class GamePart extends StatelessWidget {
  GamePart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) {
        controller.startTime();
        return Scaffold(
          body: SafeArea(
            bottom: false,
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Container(
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Image.asset("assets/images/pan.png"),
                        Text(
                          "${controller.currentQuestion}",
                          textAlign: TextAlign.center,
                          style: TextConfig.getSimpleTextStyle(true,
                              color: Colors.black, size: 4),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height,
                        ),
                      ],
                    ),
                  ),
                ),
                //Top bar
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    color: Theme.of(context).backgroundColor,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    child: Row(
                      children: [
                        LeadButton(
                          text: "${controller.userLevel}".toUpperCase(),
                        ),
                        Spacer(),
                        LifeProgressor(),
                        Spacer(),
                        InkWell(
                          onTap: () {
                            controller.quitPart();
                          },
                          child: LeadButton(
                            text: "x",
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                //Bottom bar
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: KeyboardWidget(),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          color: Theme.of(context).backgroundColor,
                          padding:
                              EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                          child: Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  showModalBottomSheet(
                                    context: context,
                                    builder: (context) {
                                      return BuyTime();
                                    },
                                  );
                                },
                                child: LeadButton(
                                  text: "+1".toUpperCase(),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(child: TimeProgressor()),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
