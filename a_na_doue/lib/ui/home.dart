import 'package:flutter/material.dart';
import 'package:flutter_tabler_icons/flutter_tabler_icons.dart';
import 'package:get/get.dart';

import '../utility/text_config.dart';
import 'home_controller.dart';
import 'widget/lead_button.dart';
import 'widget/lead_stat.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) {
        return Scaffold(
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    TextConfig.appName,
                    textAlign: TextAlign.center,
                    style: TextConfig.getSimpleTextStyle(true, size: 1),
                  ),
                  Expanded(
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Image.asset("assets/images/pan.png"),
                        Positioned(
                          top: 50,
                          right: 30,
                          left: 30,
                          child: Text(
                            controller.userLevel.toString(),
                            textAlign: TextAlign.center,
                            style: TextConfig.getSimpleTextStyle(true,
                                size: 4, color: Colors.yellow),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      LeadStat(
                        wid: Icon(Icons.favorite, size: 50, color: Colors.red),
                        text: controller.userLife.toString(),
                      ),
                      LeadStat(
                        wid: Icon(
                          Icons.watch_later,
                          size: 50,
                        ),
                        text: controller.userRecord.toString(),
                      ),
                      LeadStat(
                        wid: Icon(
                          TablerIcons.cookie,
                          size: 50,
                        ),
                        text: controller.userCoins.toString(),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  InkWell(
                    onTap: () {
                      controller.startGame();
                    },
                    child: LeadButton(
                      text: "Continuer".toUpperCase(),
                    ),
                  ),
                  SizedBox(height: 10),
                  InkWell(
                    onTap: () {
                      controller.restartGame();
                    },
                    child: LeadButton(
                      text: "Recommencer".toUpperCase(),
                    ),
                  ),
                  SizedBox(height: 10),
                  InkWell(
                    onTap: () {
                      _quitBox(context, controller);
                    },
                    child: LeadButton(
                      text: "Quitter".toUpperCase(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  _quitBox(dynamic context, dynamic controller) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
            child: Column(
              children: [
                Text(
                  "Quitter le jeu ?",
                  textAlign: TextAlign.center,
                  style: TextConfig.getSimpleTextStyle(true),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: LeadButton(
                        text: "Rester".toUpperCase(),
                      ),
                    ),
                    Spacer(),
                    InkWell(
                      onTap: () {
                        controller.quitGame();
                      },
                      child: LeadButton(
                        text: "Quitter".toUpperCase(),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
