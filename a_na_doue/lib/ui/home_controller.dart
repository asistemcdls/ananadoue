import 'dart:async';
import 'dart:io';

import 'package:a_na_doue/model/parts.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HomeController extends GetxController {
  final userData = GetStorage();

  late int userLife;
  late int userLevel;
  late int userRecord;
  late int userCoins;

  late Timer partTimer;
  double? timePercent = 0.0;
  int? time = 0;
  int? totalTime = 60 * 5;

  late TextEditingController textEditingController;

  var currentQuestion;
  var currentResponse;

  @override
  void onInit() {
    super.onInit();
    textEditingController = TextEditingController();

    initGameVariables();
  }

  @override
  onClose() {
    super.onClose();
    partTimer.cancel();
    timePercent = 0.0;
    time = 0;
    textEditingController.clear();
  }

  initGameVariables() {
    //Write in local database
    userData.writeIfNull('level', 1);
    userData.writeIfNull('life', 5);
    userData.writeIfNull('record', 0);
    userData.writeIfNull('coin', 0);

    //Initilization of all variables
    userLife = userData.read('life');
    userLevel = userData.read('level');
    userRecord = userData.read('record');
    userCoins = userData.read('coin');

    //Question & responses
    currentQuestion = listPart[0]['q'];
    currentResponse = listPart[0]['r'];
  }

  rewriteGameVariables() {
    //Write in local database
    userData.write('level', 1);
    userData.write('life', 5);
    userData.write('record', 0);
    userData.write('coin', 0);

    //Initilization of all variables
    userLife = userData.read('life');
    userLevel = userData.read('level');
    userRecord = userData.read('record');
    userCoins = userData.read('coin');

    update();
    //Question & responses
    currentQuestion = listPart[0]['q'];
    currentResponse = listPart[0]['r'];
    update();
  }

  //Continuous
  startGame() {
    Get.toNamed('/part');
  }

  //Want to Restart Game
  restartGame() {
    initGameVariables();
    rewriteGameVariables();
    update();
    Get.toNamed('/part');
  }

  //Want to quit game
  quitGame() {
    exit(0);
  }

  //Want to quit game part
  quitPart() {
    partTimer.cancel();
    timePercent = 0.0;
    time = 0;
    update();
    Get.back();
  }

  //Start part
  startTime() {
    partTimer = Timer(Duration(seconds: 1), () {
      timePercent = timePercent! + 0.01;
      time = time! + 1;
      update();

      if (timePercent! >= 1.0) {
        timePercent = 0.00;
        update();
        diminishedLife();
      }

      if (time! >= totalTime!) {
        time = 0;
        partTimer.cancel();
        update();
        rewriteGameVariables();
        Get.back();
      }
    });
  }

  //Buy Life
  buyLife() {
    userLife += 1;
    userData.write('life', userLife);

    userCoins -= 10;
    userData.write('coin', userCoins);
    //All update
    update();
  }

  //Buy Time
  buyTime() {
    totalTime = totalTime! + 10;
    //All update
    update();
  }

  //Diminished life
  diminishedLife() {
    userLife -= 1;

    update();
    userData.write('life', userLife);
    update();
    if (userLife <= 0) {
      userLevel -= 1;
      update();
      userData.write('level', userLevel);
      update();
      Get.back();
    }
  }

  check() {
    if (textEditingController.text.toString() == currentResponse) {
      userLevel += 1;
      userCoins += 10;
      update();
      userData.write('level', userLevel);
      userData.write('coin', userCoins);
      update();

      currentQuestion = listPart[userLevel]['q'];
      currentResponse = listPart[userLevel]['r'];

      textEditingController.clear();

      quitPart();
    } else {
      quitPart();
    }
  }
}
