import 'package:a_na_doue/ui/home_controller.dart';
import 'package:a_na_doue/utility/text_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tabler_icons/flutter_tabler_icons.dart';
import 'package:get/get.dart';

import 'lead_button.dart';

class BuyLife extends StatelessWidget {
  const BuyLife({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) {
        return SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset(
                      "assets/images/h_p.png",
                      width: 50,
                      height: 60,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Acheter une vie ?",
                                textAlign: TextAlign.center,
                                style: TextConfig.getSimpleTextStyle(true,
                                    family: 'Gl'),
                              ),
                              Spacer(),
                              Text(
                                "${controller.userLife} / 5",
                                style: TextConfig.getSimpleTextStyle(true),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                            ),
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                gradient: LinearGradient(
                                  colors: [
                                    Colors.blue,
                                    Colors.blueAccent,
                                    Colors.blue,
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Text(
                            "Possibilité : "
                            "${controller.userLife >= 5 ? " 0 car vie pleine ou coins insuffisant" : 1}",
                            style: TextConfig.getSimpleTextStyle(true,
                                family: 'Gl'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Icon(
                      TablerIcons.cookie,
                      size: 50,
                      color: Colors.tealAccent,
                    ),
                    Text(
                      "${controller.userCoins}",
                      style: TextConfig.getSimpleTextStyle(
                        true,
                        color: Colors.amberAccent,
                      ),
                    ),
                    Spacer(),
                    controller.userLife < 5 && controller.userCoins > 10
                        ? InkWell(
                            onTap: () {
                              controller.buyLife();
                              Get.back();
                            },
                            child: LeadButton(
                              text: "Acheter".toUpperCase(),
                            ),
                          )
                        : SizedBox(),
                    SizedBox(
                      width: 10,
                    ),
                    InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: LeadButton(
                        text: "Non".toUpperCase(),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
