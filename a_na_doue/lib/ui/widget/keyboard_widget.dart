import 'package:a_na_doue/ui/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'lead_button.dart';

class KeyboardWidget extends StatelessWidget {
  const KeyboardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) {
        return Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.all(0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.green,
                ),
                child: Container(
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    controller: controller.textEditingController,
                    decoration: InputDecoration(border: InputBorder.none),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.blue,
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                controller.check();
              },
              child: LeadButton(
                text: "ok".toUpperCase(),
              ),
            ),
          ],
        );
      },
    );
  }
}
