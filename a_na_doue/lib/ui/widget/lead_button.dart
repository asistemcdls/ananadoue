import 'package:flutter/material.dart';

import '../../utility/text_config.dart';

// ignore: must_be_immutable
class LeadButton extends StatefulWidget {
  String? text;

  LeadButton({this.text, Key? key}) : super(key: key);

  @override
  _LeadButtonState createState() => _LeadButtonState();
}

class _LeadButtonState extends State<LeadButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
      ),
      child: Container(
        child: Center(
          child: Text(
            widget.text!,
            style: TextConfig.getSimpleTextStyle(true),
          ),
        ),
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            colors: [
              Colors.blue,
              Colors.blueAccent,
              Colors.blue,
            ],
          ),
        ),
      ),
    );
  }
}
