import 'package:flutter/material.dart';

import '../../utility/text_config.dart';

// ignore: must_be_immutable
class LeadStat extends StatelessWidget {
  String? text;
  dynamic wid;

  LeadStat({this.text, this.wid, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        wid,
        Text(
          text!,
          textAlign: TextAlign.center,
          style: TextConfig.getSimpleTextStyle(true),
        ),
      ],
    );
  }
}
