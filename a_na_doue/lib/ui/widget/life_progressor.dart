import 'package:a_na_doue/ui/home_controller.dart';
import 'package:a_na_doue/ui/widget/buy_life.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LifeProgressor extends StatelessWidget {
  const LifeProgressor({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) {
        return InkWell(
          onTap: () {
            buyLife(context, controller);
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                1 <= controller.userLife
                    ? "assets/images/h_p.png"
                    : "assets/images/h_v.png",
                width: 30,
                height: 30,
              ),
              Image.asset(
                2 <= controller.userLife
                    ? "assets/images/h_p.png"
                    : "assets/images/h_v.png",
                width: 30,
                height: 30,
              ),
              Image.asset(
                3 <= controller.userLife
                    ? "assets/images/h_p.png"
                    : "assets/images/h_v.png",
                width: 30,
                height: 30,
              ),
              Image.asset(
                4 <= controller.userLife
                    ? "assets/images/h_p.png"
                    : "assets/images/h_v.png",
                width: 30,
                height: 30,
              ),
              Image.asset(
                5 == controller.userLife
                    ? "assets/images/h_p.png"
                    : "assets/images/h_v.png",
                width: 30,
                height: 30,
              )
            ],
          ),
        );
      },
    );
  }

  buyLife(BuildContext context, dynamic controller) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return BuyLife();
      },
    );
  }
}
