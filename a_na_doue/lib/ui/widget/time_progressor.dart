import 'package:a_na_doue/ui/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TimeProgressor extends StatelessWidget {
  TimeProgressor({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) {
        return Container(
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Container(
            height: 10,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                colors: [
                  Colors.blue,
                  Colors.blueAccent,
                  Colors.blue,
                ],
              ),
            ),
            child: LinearProgressIndicator(
              value: controller.timePercent!.toDouble(),
              backgroundColor: Colors.pinkAccent,
              color: Colors.amber,
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.teal),
            ),
          ),
        );
      },
    );
  }
}
