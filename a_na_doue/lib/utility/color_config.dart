import 'package:flutter/material.dart';
import 'package:stacked_themes/stacked_themes.dart';

class ColorConfig {
  static Color? inboxBackgroundDark = Colors.grey[800];

  static Color infoBoxColor = Colors.blue;
  static Color buyButtonColor = Colors.green;

  static Color cozcasInfoLabel = Colors.orange;
}
