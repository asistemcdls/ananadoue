import 'package:flutter/material.dart';
import 'package:stacked_themes/stacked_themes.dart';

class TextConfig {
  static String appName = "A nan â Doué";

  static TextStyle getSimpleTextStyle(bool bold,
      {Color? color, int? size, String? family}) {
    return TextStyle(
        fontFamily: family != null
            ? family
            : size == 1
                ? 'Beautiful'
                : 'Sweet',
        fontWeight: bold ? FontWeight.bold : FontWeight.normal,
        fontSize: size != null
            ? (size == 3
                ? 10
                : size == 1 || size == 4
                    ? 50
                    : null)
            : null,
        color: color != null ? color : null);
  }

  static String loading = "En cours...";
  static String finished = "Terminé";
  static String newPart = "Nouvelle partie";
  static String failure = "Echec";
}
